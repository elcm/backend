import { db } from './db';
import * as httpError from 'http-errors';
// import * as bcrypt from 'bcryptjs';
import {
  Utils,
  Users,
  UserNode
} from '../lib';
// import * as jwt from 'jsonwebtoken';
import * as util from 'util';

// /**
//  * A processed user node.
//  * @typedef {Object} UserNode
//  * @property {string} Email - Email address
//  * @property {datetime} Created - Created date/time
//  * @property {boolean} Active - Whether or not the user is active.
//  */

export interface RunResponseNode {
  _id: string,
  CharacterLink: 'string',
  TagLine: 'string',
  RPResponse: 'string',
  Created: 'datetime',
  Owner: UserNode
}

class RunResponseFunctions {
  constructor() {
    this.parseRunResponseNode = this.parseRunResponseNode.bind(this);
    this.parseRunResponseNodes = this.parseRunResponseNodes.bind(this);
    this.parseRunResponseNodeFromRecord = this.parseRunResponseNodeFromRecord.bind(this);
    this.parseRunResponseNodesFromRecords = this.parseRunResponseNodesFromRecords.bind(this);
    this.parseRunResponseNodesFromQueryResult = this.parseRunResponseNodesFromQueryResult.bind(this);
    this.parseRunResponseNodeFromRecord = this.parseRunResponseNodeFromRecord.bind(this);
    this.parseRunResponseNodeFromQueryResult = this.parseRunResponseNodeFromQueryResult.bind(this);

    this.getForRun = this.getForRun.bind(this);

    this.Create = this.Create.bind(this);
    this.Update = this.Update.bind(this);
    this.Delete = this.Delete.bind(this);
  }


  public parseRunResponseNode(response: any): Promise<RunResponseNode> {
    if (!response)
      return null;

    if (typeof response.Fields == 'string')
      response.Fields = JSON.parse(response.Fields);

    return Promise.all([
      Utils.parseNode(response),
      Utils.parseNode(response.Owner)
    ])
    .then((args) => {
      args[0].Owner = args[1];
      if (typeof args[0].Fields == 'string')
        args[0].Fields = JSON.parse(args[0].Fields);
      return args[0];
    })
  }

  public parseRunResponseNodes(data: any): Promise<RunResponseNode[]> {
    if (data.records)
      return this.parseRunResponseNodesFromQueryResult(data);
    else if (data.length && data[0]._fieldLookup)
      return this.parseRunResponseNodesFromRecords(data);
    else
      return data.map(this.parseRunResponseNode);
  }

  private parseRunResponseNodesFromQueryResult(res: any): Promise<RunResponseNode[]> { return this.parseRunResponseNodesFromRecords(res.records); }
  private parseRunResponseNodesFromRecords(records: any): Promise<RunResponseNode[]> { return Promise.all(records.map(this.parseRunResponseNodeFromRecord));  }

  private parseRunResponseNodeFromRecord(record: any): Promise<RunResponseNode> {
    if (record._fieldLookup && typeof record._fieldLookup.s == 'number') {
      let node = record._fields[record._fieldLookup.s].properties;
      if (typeof record._fieldLookup.u == 'number')
        node.Owner = record._fields[record._fieldLookup.u].properties;

      return this.parseRunResponseNode(node);
    } else {
      console.log(record);
      throw new Error("Unrecognized record.");
    }
  }

  private parseRunResponseNodeFromQueryResult(res: any): Promise<RunResponseNode> {
    if (res.records && res.records.length)
      return this.parseRunResponseNodeFromRecord(res.records[0]);

    return null;
  }


  public getForRun(id: string): Promise<RunResponseNode[]> {
    return db.cypher(
        "MATCH (r:Run {_id: {run}})<-[:Run]-(s:RunResponse)-[:User]->(u:User)\n" +
        "RETURN r,u,s",
        {
          run: id
        }
      )
      .then(this.parseRunResponseNodesFromQueryResult)
      ;
  }

  public getById(id: string): Promise<RunResponseNode> {
    return db.cypher(
      "MATCH (s:RunResponse {_id: {id}})-[:User]->(u:User)\n" +
      "RETURN s,u",
      {
        id: id
      }
    )
    .then((res) => {
      if (res.records && res.records.length)
        return this.parseRunResponseNodeFromRecord(res.records[0]);
      else
        throw new httpError(404, "Run response not found.");
    })
    ;
  }

  public Create(run: string, owner: string, settings: any): Promise<RunResponseNode> {
    settings.Created = Date.now();
    settings._id = Utils.generateID();

    if (typeof settings.Fields == 'object')
      settings.Fields = JSON.stringify(settings.Fields);

    return db.cypher(
      "MATCH (u:User {_id: {owner}})\n" +
      "MATCH (r:Run {_id: {run}})\n" +
      "CREATE (u)<-[:User]-(s:RunResponse)-[:Run]->(r)\n" +
      "SET s={props}\n" +
      "RETURN r,u,s",
      {
        owner: owner,
        run: run,
        props: settings
      }
    )
    .then((res) => this.parseRunResponseNodeFromRecord(res.records[0]))
    ;
  }

  public Update(run: string, settings: any, allowPartial: boolean = true): Promise<RunResponseNode> {
    let chain: Promise<any> = db.first('RunResponse', '_id', run);

    chain = chain.then((res) => {
      settings._id = res.get('_id');
      settings.Created = res.get('Created');
      if (typeof settings.Fields == 'object') {
        settings.Fields.forEach((field) => {
          if (!field._id)
            field._id = Utils.generateID()
        });
        settings.Fields = JSON.stringify(settings.Fields);
      }
      return res;
    })

    if (allowPartial) {
      chain = chain.then((res) => {
        return res.update(settings);
      })
      .then((res) => {
        console.log("Update: ", res);
        return res;
      })
      .then(this.parseRunResponseNode)
      ;
    } else {
      chain = chain.then((res) => {

        return db.cypher(
          "MATCH (r:Run)<-[:Run]-(s:RunResponse {_id: {id}})-[:User]->(u:User)\n" +
          "SET s = {props}\n" +
          "RETURN r,u,s",
          {
            id: run,
            props: settings
          }
        );
      })
      .then(this.parseRunResponseNodeFromQueryResult)
      ;
    }




    return chain;
  }

  public Delete(run: string): Promise<void> {
    return db.cypher(
        "MATCH (s:RunResponse {_id: {id}})\n" +
        "DETACH DELETE s",
        {
          id: run
        }
      )
      .then(() => null)
      ;
  }
}


export default new RunResponseFunctions();