import { db } from './db';
import * as httpError from 'http-errors';
import * as bcrypt from 'bcryptjs';
import { Utils } from '../lib';
import * as jwt from 'jsonwebtoken';

/**
 * A processed user node.
 * @typedef {Object} UserNode
 * @property {string} Email - Email address
 * @property {datetime} Created - Created date/time
 * @property {boolean} Active - Whether or not the user is active.
 */

export interface UserNode {
  _id: string,
  Email: string;
  Created: Date;
  Active: boolean;
  Role?: any;
}


class UserFunctions {

  public constructor() {
    this.parseUserNode = this.parseUserNode.bind(this);
    this.parseUserNodes = this.parseUserNodes.bind(this);
    this.parseUserNodeFromRecord = this.parseUserNodeFromRecord.bind(this);
    this.parseUserNodesFromRecords = this.parseUserNodesFromRecords.bind(this);
    this.parseUserNodesFromQueryResult = this.parseUserNodesFromQueryResult.bind(this);

    this.addRoleToUser = this.addRoleToUser.bind(this);

    this.getUsers = this.getUsers.bind(this);
    this.getById = this.getById.bind(this);
    this.getAuthenticatedUser = this.getAuthenticatedUser.bind(this);
    this.getUsersForRun = this.getUsersForRun.bind(this);

    this.findByEmail = this.findByEmail.bind(this);
    this.findByEmailAndPassword = this.findByEmailAndPassword.bind(this);

    this.generateUserToken = this.getAuthenticatedUser.bind(this);

    this.Create = this.Create.bind(this);
    this.Update = this.Update.bind(this);
    this.Delete = this.Delete.bind(this);
  }

  /**
   * Processes a node returned from neo4j into
   * a simple JSON representation, for consumption
   * by the front-end.
   * Parameters:
   *   @param {*} user - The user to parse.
   * Returns:
   *   @return {Promise<UserNode>} A processed user node.
   */
  public parseUserNode(user: any, includeRole: boolean = true): Promise<UserNode> {
    // console.log("Parsing: ", user);
    // Attach Role
    let chain = Utils.parseNode(user);

    if (includeRole) {
      chain = chain.then((User) => {
        // console.log("Adding role: ", User);
        return Promise.all([
          User,
          db.cypher(
            "MATCH (u:User {_id: {user}})-[:Role]->(r:Role)\n" +
            "RETURN u,r",
            {user: User._id}
          )
        ])
      })
      .then((args) => {
        // console.log("Role: ", args[1].records[0]);
        delete args[0].Password;
        let parsedUser = this.addRoleToUser(args[0], args[1]);
        return parsedUser;
      })
      ;
    }

    return chain;
  }

  private addRoleToUser(user: UserNode, role: any): UserNode {
    user.Role = null;
    // console.log("Adding role: ", role);
    
    if (role.records)
      role = role.records[0] || null;

    if (!role)
      return user;
    
    if (role._fields && role._fieldLookup && role._fields[role._fieldLookup.r]) {
      user.Role = role._fields[role._fieldLookup.r].properties;
    } else {
      user.Role = role;
    }

    return user;
  }

  public parseUserNodes(users: any, includeRoles: boolean = true): Promise<UserNode[]> {
   
    let chain: Promise<UserNode[]> = Promise.resolve(users); 
    // Coerce type

    // Query result
    if (users.records)
      chain = chain.then(() => this.parseUserNodesFromQueryResult(users));
    // List of records from a query result (ie: result.records)
    else if (users.length && users[0]._fieldLookup)
      chain = chain.then(() => this.parseUserNodesFromRecords(users));
    // List of property objects
    else
      chain = chain.then(() => users.map(this.parseUserNode));

    // Add roles
    if (includeRoles) {
      chain = chain.then((nodes) => {
        return Promise.all([
          nodes,
          db.cypher(
            "MATCH (u:User)-[:Role]->(r:Role)\n" +
            "WHERE u._id IN {ids}\n" +
            "RETURN u,r",
            {
              ids: nodes.map((node) => node._id)
            }
          )
        ])
      })
      .then((args) => {
        let nodes: UserNode[] = args[0];
        let roles = args[1];

        // console.log("Role: ", roles.records[0]);

        roles.records.forEach((role: any) => {
          let user = nodes.find((node) => node._id == role._fields[role._fieldLookup.u].properties._id)
          if (user) {
            this.addRoleToUser(user, role);
          }
        });




        return nodes;
      })
    }

    // chain = chain.then(() => []);
    return chain;
  }

  private parseUserNodesFromQueryResult(res: any): Promise<UserNode[]> { return this.parseUserNodesFromRecords(res.records); }
  private parseUserNodesFromRecords(records: any): Promise<UserNode[]> { return Promise.all(records.map(this.parseUserNodeFromRecord)); }

  private parseUserNodeFromRecord(record: any): Promise<UserNode> {
    if (record._fieldLookup && typeof record._fieldLookup.u == 'number') {
      let node = record._fields[record._fieldLookup.u].properties;
      return this.parseUserNode(node, false);
    } else {
      throw new Error("Unrecognized record.");
    }
  }

  /**
   * Returns a list of users who have responded to a run.
   */
  public getUsersForRun(run: string): Promise<UserNode[]> {
    return db.cypher(
       "MATCH (u:User)<-[:User]-(s:RunResponse)-[:Run]->(r:Run {_id: {run}})\n" +
       "RETURN u,r,s",
       {
         run: run
       }
      )
      .then(this.parseUserNodes)
      ;
  }

  public getById(id: string): Promise<UserNode> {
    // console.log("Getting: ", id);
    return db.cypher(
        "MATCH (u:User {_id: {id}})\n" +
        "RETURN u",
        {
          id: id
        }
      )
      .then((res) => {
        if (res.records && res.records.length)
          return this.parseUserNode(res.records[0])
        else
          return null;
      })
      ;
  }

  /**
   * Returns the first user that matches a given email address.
   * Parameters:
   *   @param {string} email - The email address to find.
   * Returns:
   *   @return {Promise<UserNode>} A promise that resolves to a processed user node.
   */
  public findByEmail(email: string): Promise<UserNode> {
    // console.log("Finding: ", email);
    return db.first('User', 'Email', email)
      .then((res) => {
        if (res)
          return this.parseUserNode(res);
        else
          return null;
      })
      ;
  }

  /**
   * Returns the first user with the given email address, verifying the supplied password.
   * Parameters:
   *   @param {string} email - The email address to find.
   *   @param {string} password - The password to verify.
   * Returns:
   *   @return {Promise<UserNode>} A promise that resolves to a processed user node.
   */
  public findByEmailAndPassword(email: string, password: string): Promise<UserNode> {
    return db.first('User', 'Email', email)
             .then((User) => {
               if (User) {
                 return Promise.all([
                   User,
                   bcrypt.compare(password, User.get('Password'))
                 ]);
               } else {
                 throw new httpError(401, "Unable to authenticate.");
               }
             })
             .then((args) => {
               if (args[1])
                 return this.parseUserNode(args[0]);
               else
                 throw new httpError(401, "Unable to authenticate.");
             });

  }


  /**
   * Retrieves list of users.
   * Parameters:
   *    @param {any} settings - Object containing keys for limit, skip
   * Returns:
   *    @return {Promise<UserNode[]>} - An array of parsed nodes
   */  
  public getUsers(settings: any): Promise<UserNode[]> {
    return db.cypher(
        "MATCH (u:User)\n" +
        "RETURN u\n" +
        "ORDER BY u.Name DESC\n" +
        "SKIP {skip}\n" +
        "LIMIT {limit}\n",
        settings
     )
    .then((res: any) => this.parseUserNodes(res))
    ;
  }

  /**
   * Creates a user.
   * Parameters:
   *    @param {any} settings - Object containing keys for email, username.
   * Returns:
   *    @return {Promise<UserNode>} - A parsed node of the newly created user.
   */
  public Create(settings: any): Promise<UserNode> {    
    // console.log("Creating: ", settings);
    let props: any = {
      Email: settings.Email,
      Password: settings.Password,
      Active: true,
      Created: Date.now(),
      _id: Utils.generateID()
    };
    let chain: any = db.create('User', props);

    chain = chain
      .then((user) => {
        let promises = [user];
        if (settings.Role) {
          promises.push(this.setUserRole(props._id, 'Player'));
        }

        return Promise.all(promises);
      });


    return chain
            .then(args => this.parseUserNode(args[0]))
            ;
    ;

    // return Promise.reject(new Error("Test"));
  }

  public setUserRole(id: string, role: string): Promise<void> {
    // console.log("Setting user role: ", id, "=", role);
    return db.cypher(
        "MATCH (r:Role)\n" +
        "WHERE r._id={role} OR r.Name={role}\n" +
        "MATCH (u:User {_id: {user}})\n" +
        "CREATE (u)-[:Role]->(r)\n" +
        "RETURN u,r",
        {
          user: id,
          role: role
        }
      )
      .then((res) => null)
      ;
  }

  /**
   * Returns a bcrypt hash of the supplied password, via Promise
   * Parameters:
   *    @param {string} password - The password to hash.
   * Returns:
   *    @return {Promise<string>} - A promise that resolves to the hashed password.
   */
  public hashPassword(password: string): Promise<string> {
    return new Promise((resolve, reject) => {
      bcrypt.hash(password, 10, (err, hash) => {
        if (err)
          reject(err);
        else
          resolve(hash);
      });
    });
  }

  public revokeToken(token: any): Promise<any> {
    return db.cypher("MATCH (t:Token {Token: {token}}) DETACH DELETE t", {token: token.token})
  }

  public lookupFromToken(token: any): Promise<UserNode> {
    // console.log(token);
    return db.cypher("MATCH (t:Token {Token: {token}})-[:User]->(u:User) return t,u", {token: token.token})
      .then((result) => {
        let record: any = result.records[0];
        
        if (result.records.length == 0)
          throw new Error("Token not found.");


        let tokenIndex = record._fieldLookup.t;
        let userIndex = record._fieldLookup.u;

        let user = record._fields[userIndex].properties;
        return this.parseUserNode(user);
      })
  }

  public getAuthenticatedUser(passport: any): (req: any, res: any, next: any) => void {
    return function(req, res, next): void {
      passport.authenticate('jwt', {session: false}, (err, user, info) => {
        if (err)
          return next(err);
        if (!user)
          return next(new Error(info));

        this.lookupFromToken(req.user)
          .then(next)
          .catch(next)
          ;
      });
    }

  }

  public Delete(id: string): Promise<void> {
    return db.cypher(
        "MATCH (u:User {_id: {id}})\n" +
        "DETACH DELETE u",
        {
          id: id
        }
      )
      .then(() => null)
      ;
  }

  public Update(id: string, settings: any, allowPartial: boolean = false): Promise<UserNode> {
    let chain: any = db.first('User', '_id', id)

    return chain.then((user) => {
        if (!user)
          throw new httpError(404, "User not found.");

        if (allowPartial) {
          return user.update(settings);
        } else {
          let props = Object.assign({}, settings, {_id: id});
          // console.log("Props: ", props);
          return db.cypher(
            "MATCH (u:User {_id: {id}})\n" +
            "SET u = {props}\n" +
            "RETURN u",
            {
              id: id,
              props: props
            }
          )
          .then((res) => res.records[0])
          ;
        }

      })
      .then(this.parseUserNode)
      ;
  }

  public generateUserToken(user: UserNode): Promise<string> {
    let id = Utils.generateID();
    let res: any = db.create('Token', {
      Token: id,
      Created: Date.now(),
      Type: 'Access'
    });

    return res.then((token) => {
      return Promise.all([
        token,
        db.first('User', '_id', user._id)
      ]);
    })
    .then((args) => {
      // console.log("JWT secret: ", process.env.JWT_SECRET);
      return Promise.all([
        args[0],          // Token
        args[1],          // User
        args[0].relateTo(args[1], 'User')
      ]);
    })
    .then((args) => { return jwt.sign({ token: args[0].get('Token')}, process.env.JWT_SECRET); })
    ;
  }
}

export default new UserFunctions();