import { db } from './db';
import * as httpError from 'http-errors';
import * as bcrypt from 'bcryptjs';


class UtilFunctions {
  constructor() {}

  public generateID(): string {
    return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16).replace(/\\/gi, "");
    });
  }

public catchAPIError(res: any, suppress: boolean = false): any {
    return function(err: any): void {
      if (err.statusCode)
        res.status(err.statusCode);
      else
        res.status(500);
      res.json({status: 'error', errors: [err.message]});

      if (!suppress)
        console.error(err);
    }
  }

  public parseNode(node: any): Promise<any> {
    if (!node)
      return Promise.resolve(null);

    let chain = Promise.resolve(node);

    if (typeof node.toJson == 'function')
      chain = chain.then((node) => node.toJson());
    else if (node.properties)
      chain = chain.then((node) => node.properties);
    else if (node._fieldLookup && typeof node._fieldLookup.u == 'number')
      chain = chain.then((node) => node._fields[node._fieldLookup.u].properties);

    return chain.then((json: any) => {

      let parsed = Object.assign({}, json);

      // console.log("Created; ", parsed.Created);

      parsed.Created = new Date(parsed.Created);

      delete parsed._labels;

      // console.log("Parsed: ", parsed);
      return parsed;
    });
  }

}

export default new UtilFunctions();