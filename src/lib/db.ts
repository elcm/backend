import * as neode from 'neode';
import * as path from 'path';
import * as fs from 'fs';
import { Utils, Users } from '../lib';

const modelDir = path.resolve("./src/models");

// console.log("Initializing neode");
// console.log(`Model directory: ${modelDir}`);

// console.log(process.env);
/** A Neode reference, providing an OGM wrapper around a Neo4j instance */
const db = neode
            .fromEnv()
            .withDirectory(modelDir)
            ;

function initialize(): Promise<any> {
  return db.cypher("MATCH (r:Role) RETURN r", {})
           .then((res) => {
             if (res.records.length == 0) {
               // No roles, create them.
               console.log("Creating default user Roles.");
               return db.cypher(
                 "CREATE (:Role {Name: 'Administrator', Description: '', _id: {admin}})\n" +
                 "CREATE (:Role {Name: 'GM', Description: '', _id: {gm}})\n" +
                 "CREATE (:Role {Name: 'Player', Description: '', _id: {player}})",
                 {
                   admin: Utils.generateID(),
                   gm: Utils.generateID(),
                   player: Utils.generateID()
                 }
               );
             } else {
               console.log("User Roles already exist.");
               return null;
             }
           })
           .then(() => {
             
             return Promise.all([
               db.cypher("MATCH (u:User)-[:Role]->(r:Role {Name: 'Administrator'}) RETURN u", {}),
               Users.hashPassword("admin12345")
             ]);
           })
           .then((args) => {
             let res = args[0];
             let password = args[1];
             if (res.records.length == 0) {
               console.log("Creating default Administrator user.");
               return db.cypher(
                 "MATCH (r:Role {Name: 'Administrator'})\n" +
                 "CREATE (u:User)-[:Role]->(r)\n" +
                 "SET u = {props}",
                 {
                   props: {
                     Name: 'Administrator',
                     Email: 'admin@admin.com',
                     Password: password,
                     Active: true
                   }
               });
             } else {
               console.log("At least one Administrator user exists.");
               return null;
             }
           })
           .catch(console.error);
}


export { db, initialize }

// console.log(db.create.toString());