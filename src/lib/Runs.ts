import { db } from './db';
import * as httpError from 'http-errors';
import * as bcrypt from 'bcryptjs';
import {
  RunResponses,
  RunResponseNode,
  Users,
  UserNode,
  Utils
} from '../lib';
import * as jwt from 'jsonwebtoken';
import * as util from 'util';

// /**
//  * A processed user node.
//  * @typedef {Object} UserNode
//  * @property {string} Email - Email address
//  * @property {datetime} Created - Created date/time
//  * @property {boolean} Active - Whether or not the user is active.
//  */

export interface RunNode {
  _id: string,
  Name: string,
  PlayerMin: number,
  PlayerMax: number,
  Communication: string,
  Location: string,
  ThreatLevel: string,
  GameTheme: string,
  GameType: string,
  Prerequisites: string,
  Description: string,
  RPPrompt: string,
  Created: number,
  Owner: UserNode,
  Fields: string[],
  Responses: RunResponseNode[]
};


class RunFunctions {
  constructor() {}


  public getRuns(settings: any): Promise<RunNode[]> {
    return db.cypher(
      "MATCH (r:Run)-[:Owner]->(u:User)\n" +
      "OPTIONAL MATCH(r)<-[:Run]-(s:RunResponse)\n" +
      "RETURN r,u,s\n" +
      "ORDER BY r.Created DESC\n" +
      "SKIP {skip} LIMIT {limit}",
      {
        skip: settings.skip || 0,
        limit: settings.limit || 10
      }
    )
    .then((res) => {


      // console.log(util.inspect(res, false, null));

      if (res.records.length == 0)
        return [];



      // Ok, so fun times.  We'll have 0 or more RunFields,
      // and 0 or more RunResponses for each Run.

      // We need to combine these.

      let linkedRecords = this.linkRunToRunProps(res);

     return Promise.all(linkedRecords.map(this.parseRunNode));
    })
    ;
  }

  private linkRunToRunProps(res): any[] {
    let recordHash = {};
    res.records.forEach((record: any) => {
      let runIndex = record._fieldLookup.r;
      let userIndex = record._fieldLookup.u;
      let responseIndex = record._fieldLookup.s;

      let run = (record._fields[runIndex] ? record._fields[runIndex].properties : null);
      let user = (record._fields[userIndex] ? record._fields[userIndex].properties : null);
      let response = (record._fields[responseIndex] ? record._fields[responseIndex].properties : null);

      if (!recordHash[run._id])
        recordHash[run._id] = run;

      if (!recordHash[run._id].Responses)
        recordHash[run._id].Responses = [];

      if (user)
        recordHash[run._id].Owner = user;

      if (response)
        recordHash[run._id].Responses.push(response);
    });

    return Object.values(recordHash);
  }

  public Create(settings: any, owner: any): Promise<RunNode> {
    let actual = Object.assign({}, settings, {Created: Date.now(), _id: Utils.generateID()});

    let ownerId='';
    if (typeof owner == 'string')
      ownerId = owner;
    else if (owner._id)
      ownerId = owner._id;

    // Parse out fields.
    if (!actual.Fields)
      actual.Fields =[];

    actual.Fields = JSON.stringify(actual.Fields.map((field) => {
      return {
        _id: Utils.generateID(),
        Name: field
      };
    }));



    // Get around the fact that db.create incorrectly reports no .then.
    let create: any =  db.create('Run', actual);
    return create
            .then((res) => {
              return Promise.all([
                res,
                db.cypher(                            // Link to owner
                  "MATCH (r:Run {_id: {run}})\n" +
                  "MATCH (u:User {_id: {user}})\n" +
                  "CREATE (r)-[:Owner]->(u)\n" +
                  "RETURN r,u",
                  {
                    run: actual._id,
                    user: ownerId
                  }
                )
              ]);
            })
            .then((args) => { return this.getById(args[0].get('_id')) })
            ;
  }

  public Delete(id: string): Promise<any> {
    return db.first('Run', '_id', id)
             .then((run) => run.delete())
             ;
  }
  public Update(id: string, settings: any, allowPartial: boolean = false): Promise<RunNode> {
    let chain: Promise<any> = Promise.resolve();

    if (allowPartial) {
      chain = chain.then(() => db.first('Run', '_id', id))
                   .then((current: any) => {
                     if (!settings._id)
                       settings._id = id;
 
                     delete settings.Owner;
                     delete settings.Responses;

                     if (settings.Fields) {
 
                       let fields = [];
                       if (typeof settings.Fields == 'string')
                         fields = JSON.parse(settings.Fields);
                       else if (typeof settings.Fields == 'object')
                         fields = settings.Fields;
   
                       fields.forEach((field) => {
                         if (!field._id)
                           field._id = Utils.generateID();
                       });
   
                       // Back to string for serialization.
                       settings.Fields = JSON.stringify(fields);
                     }


                     settings.Created = current.Created;

                     return current.update(settings)
                   })
                   .then(() => this.getById(id))
                   ;
    } else {
      chain = chain.then(() => {
        let props = Object.assign({}, settings, {_id: id});
        if (typeof props.Fields == 'object')
          props.Fields = JSON.stringify(props.Fields);

        // console.log("Props: ", props);
        return db.cypher(
          "MATCH (r:Run {_id: {id}})\n" +
          "SET r = {props}\n" +
          "RETURN r",
          {
            id: id,
            props: props
          }
        );
      })
      .then(() => this.getById(id))
      ;
    }

    return chain;
  }

  public getById(id: string): Promise<RunNode> {
    // console.log("Getting: ", id);
    return db.cypher(
      "MATCH (r:Run {_id: {id}})-[:Owner]->(u:User)\n" +
      "OPTIONAL MATCH (r)<-[:Run]-(s:RunResponse)\n" +
      "RETURN r,u,s",
      {
        id: id
      }
    )
    .then(this.linkRunToRunProps)
    .then((records) => {
      if (records.length)
        return this.parseRunNode(records[0])

      throw new httpError(404, `Run ${id} not found.`);
    })
    ;
  }


  public parseRunNode(node: any): Promise<RunNode> {
    // console.log("Parsing: ", node);
    return Utils.parseNode(node)
            .then((Run) => {
              // console.log("Parsed: ", Run);
              if (!Run.Fields)
                Run.Fields = [];
              else if (typeof Run.Fields == 'string')
                Run.Fields = JSON.parse(Run.Fields);
              if (!Run.Responses)
                Run.Responses = [];

              return Promise.all([
                Run,
                Promise.all(Run.Responses.map(RunResponses.parseRunResponseNode)),
                Users.parseUserNode(Run.Owner)
              ]);
            })
            .then((args) => {
              // args[0].Fields = args[1];
              args[0].Responses = args[1];
              args[0].Owner = args[2];

              return args[0];
            })
            ;
  }
}


export default new RunFunctions();