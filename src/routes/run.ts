
import * as express from 'express';
import { check, validationResult } from 'express-validator';
import * as jwt from 'jsonwebtoken';
import { Utils, Users, UserNode, RunNode, Runs } from '../lib';
// import { passport} from '../passport';
import * as httpError from 'http-errors';


 /** Creates and returns router. */
function Hook(app: any): any {

  /** Express router */
  const router = express.Router();
  const passport = app.passport;

  const postValidator = [
    check('Name').not().isEmpty().trim().escape(),
    check('Description').not().isEmpty().trim().escape()
  ];

  // Routes
  // GET    /api/run     -- lists runs
  // POST   /api/run     -- Creates new run
  // GET    /api/run/:id -- returns specified run
  // PUT    /api/run/:id -- Updates a run
  // DELETE /api/run/:id -- Deletes a run
  // All routes require authentication.


  // router.use(passport.authenticate('jwt', {session: false}));
  // router.use(Users.getAuthenticatedUser(app.passport));

  router.route('/')
    .get(passport.authenticate('jwt', {session: false}), (req, res, next) => {
      // console.log("User: ", req.user);
      let settings: any = {
        skip: req.query.skip || 0,
        limit: req.query.limit || 0
      }

      Runs.getRuns(settings)
        .then((runs) => {
          res.json({status: 'success', runs: runs});
        })
        .catch(Utils.catchAPIError(res))
        ;
    })
    .post(passport.authenticate('jwt', { session: false }), postValidator, (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({status: 'error', errors: errors.array()});
      } else {
        if (!req.user || !req.user.Role || !req.user.Role.Name || (req.user.Role.Name != 'Administrator' && req.user.Role.Name != 'GM'))
          return res.status(401).json({status: 'error', errors: ["Unauthorized"]});
       
        Runs.Create(req.body, req.user)
        .then((Run) => {
          res.json({status: 'success', run: Run});
        })
        .catch(Utils.catchAPIError(res))
        ;
      }
    })
    ;

  router.route('/:id')
    .get(passport.authenticate('jwt', {session: false}), (req, res, next) => {
      Runs.getById(req.params.id)
        .then((Run) => {
          res.json({status: 'success', run: Run});
        })
        .catch(Utils.catchAPIError(res))
        ;
    })
    .put(passport.authenticate('jwt', {session: false}), (req, res, next) => {
      // TODO: Validate perms
      Runs.getById(req.params.id)
        .then((Run) => {
          if (Run.Owner._id != req.user._id && req.user.Role.Name != 'Administrator')
            throw new httpError(401, "Unauthorized");
          return Runs.Update(req.params.id, req.body, false);
        })
        .then((Run) => {
          res.json({status: 'success', run: Run});
        })
        .catch(Utils.catchAPIError(res))
        ;
    })
    .patch(passport.authenticate('jwt', {session: false}), (req, res, next) => {
      Runs.getById(req.params.id)
        .then((Run) => {
          if (Run.Owner._id != req.user._id && req.user.Role.Name != 'Administrator')
            throw new httpError(401, "Unauthorized");
          return Runs.Update(req.params.id, req.body, true);
        })
        .then((Run) => {
          res.json({status: 'success', run: Run});
        })
        .catch(Utils.catchAPIError(res))
        ;
    })
    .delete(passport.authenticate('jwt', {session: false}), (req, res, next) => {
      Runs.getById(req.params.id)
        .then((Run) => {
          if (Run.Owner._id != req.user._id && req.user.Role.Name != 'Administrator')
            throw new httpError(401, "Unauthorized");

          return Runs.Delete(req.params.id)
        })
        .then(() => {
          res.json({status: 'success'});
        })
        .catch(Utils.catchAPIError(res))
        ;
    })
    ;

  router.route('/:id/users')
    .get(passport.authenticate('jwt', {session: false}), (req, res, next) => {
      Users.getUsersForRun(req.params.id)
        .then((users) => {
          res.json({status: 'success', users: users});
        })
        .catch(Utils.catchAPIError(res))
        ;
    })






  /** Return our Express router, to be hooked to the application. */
  return router;
}

export default Hook;