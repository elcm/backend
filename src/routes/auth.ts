
/**
 * Configures router middleware for authentication routes.<br>
 * __Unauthenticated routes:__
 * - /register - Handles user registration.<br>
 * - /login - Handles logging in.
 *
 * __Authenticated routes:__
 * - /logout - Logging out.<br>
 * - /me - Current user.<br>
 */

import * as express from 'express';
import { check, validationResult } from 'express-validator';
import * as jwt from 'jsonwebtoken';
import { Utils, Users, UserNode } from '../lib';
// import { passport} from '../passport';
import * as httpError from 'http-errors';

 /** Creates and returns router. */
function Hook(app: any): any {

  /** Express router */
  const router = express.Router();


  /** Validation settings for registration route */
  const registrationValidation = [
    check('email').isEmail(),
    check('password').isLength({ min: 5})
  ];


  // Unauthenticated routes.

  /** Registration route.*/
  router.post("/register", registrationValidation, (req, res) => {
    // console.log("/register");
    /** Validate our input, returning any errors. */
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      // console.log("Validation error: ", errors);
      return res.status(422).json({status: 'error', errors: errors.array()});
    } else {
      // console.log("Registering ...", req.body);
      Users.findByEmail(req.body.email)
        .then((User?: UserNode) => {
          if (User)
            throw new httpError(401, 'Username is already registered.');
          return Users.hashPassword(req.body.password);
        })
        .then((password: string) => {
          // console.log(password);
          return Users.Create({
            Email: req.body.email,
            Password: password
          });
        })
        .then((User) => {
          res.json({status: 'success'});
        })
        .catch(Utils.catchAPIError(res))
        ;
    }
  });

  /** Login route.*/
  router.post("/login", app.passport.authenticate('local', {session: false}), (req, res, next) => {
    // console.log("/login");
    if (!req.user) {
      res.status(422).json({status: 'error', errors: ["Unable to authenticate."]});
      return;
    }

    // Users.generate
    Users.generateUserToken(req.user)
    .then((Token) => {
      if (Token)
        res.json({status: 'success', token: Token});
      else
        throw new Error("Unable to create token.");
    })
    .catch(Utils.catchAPIError(res))
    ;
  });

  // Authenticated routes.
  router.post("/logout", app.passport.authenticate('jwt', { session: false}), (req, res, next) => {
    let token = req.user;
    if (!token)
      return next(new httpError(401, "Not authenticated."));

    Users.revokeToken(token)
      .then((result) => {
        res.json({status: 'success'});
      })
      .catch(Utils.catchAPIError(res))
      ;
  });

  router.get('/me', app.passport.authenticate('jwt', {session: false}), (req, res, next) => {
    // console.log(app.passport._framework.authenticate.toString());
    if (!req.user)
      return next(new httpError(401, "Unable to authenticate."));

    res.json({status: 'success', user: req.user});
  });






  /** Return our Express router, to be hooked to the application. */
  return router;
}

export default Hook;