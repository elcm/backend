
import * as express from 'express';
// import { check, validationResult } from 'express-validator';
// import * as jwt from 'jsonwebtoken';
import { Utils, RunResponses, RunResponseNode } from '../lib';
// import { passport} from '../passport';
import * as httpError from 'http-errors';


 /** Creates and returns router. */
function Hook(app: any): any {

  /** Express router */
  const router = express.Router();
  const passport = app.passport;

  router.route('/run/:id')
    .get(passport.authenticate('jwt', {session: false}), (req, res, next) => {
      RunResponses.getForRun(req.params.id)
        .then((responses) => {
          res.json({status: 'success', responses: responses});
        })
        .catch(Utils.catchAPIError(res))
        ;
    })
    .post(passport.authenticate('jwt', {session: false}), (req, res, next) => {
      RunResponses.Create(req.params.id, req.user._id, req.body)
        .then((response) => {
          res.json({status: 'success', response: response});
        })
        .catch(Utils.catchAPIError(res))
        ;
    })

  router.route('/:id')
    .get(passport.authenticate('jwt', {session: false}), (req, res, next) => {
      RunResponses.getById(req.params.id)
        .then((response) => {
          res.json({status: 'success', response: response});
        })
        .catch(Utils.catchAPIError(res))
        ;
    })
    .put(passport.authenticate('jwt', {session: false}), (req, res, next) => {
      RunResponses.getById(req.params.id)
        .then((response) => {
          if (!response)
            throw new httpError(404, "Response not found.");
          else if ((req.user.Role && req.user.Role.Name == 'Administrator') || (response.Owner && response.Owner._id == req.user._id))
            return RunResponses.Update(req.params.id, req.body, false);
          else
            throw new httpError(403, "Unauthorized.");
        })
        .then((response) => {
          res.json({status: 'success', response: response});
        })
        .catch(Utils.catchAPIError(res))
        ;
    })
    .patch(passport.authenticate('jwt', {session: false}), (req, res, next) => {
      RunResponses.getById(req.params.id)
        .then((response) => {
          if (!response)
            throw new httpError(404, "Response not found.");
          else if ((req.user.Role && req.user.Role.Name == 'Administrator') || (response.Owner && response.Owner._id == req.user._id))
            return RunResponses.Update(req.params.id, req.body, true);
          else
            throw new httpError(403, "Unauthorized.");
        })
        .then((response) => {
          res.json({status: 'success', response: response});
        })
        .catch(Utils.catchAPIError(res))
        ;
    })
    .delete(passport.authenticate('jwt', {session: false}), (req, res, next) => {
      RunResponses.getById(req.params.id)
        .then((response) => {
          if (!response)
            throw new httpError(404, "Response not found.");
          if ((req.user.Role && req.user.Role.Name == 'Administrator') || (response.Owner && response.Owner._id == req.user._id))
            return RunResponses.Delete(req.params.id);
          else
            throw new httpError(403, "Unauthorized.");
        })
        .then(() => {
          res.json({status: 'success'});
        })
        .catch(Utils.catchAPIError(res))
        ;
    });


    // .get(passport.authenticate('jwt', {session: false}), (req, res, next) => {
    //   // console.log("User: ", req.user);
    //   let settings: any = {
    //     skip: req.query.skip || 0,
    //     limit: req.query.limit || 0
    //   }

    //   Runs.getRuns(settings)
    //     .then((runs) => {
    //       res.json({status: 'success', runs: runs});
    //     })
    //     .catch(Utils.catchAPIError(res))
    //     ;
    // })





  /** Return our Express router, to be hooked to the application. */
  return router;
}

export default Hook;