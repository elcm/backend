
import * as express from 'express';
// import { check, validationResult } from 'express-validator';
// import * as jwt from 'jsonwebtoken';
import { Utils, Users, UserNode } from '../lib';
// import { passport} from '../passport';
import * as httpError from 'http-errors';


 /** Creates and returns router. */
function Hook(app: any): any {

  /** Express router */
  const router = express.Router();
  const passport = app.passport;

  router.route('/')
    .get(passport.authenticate('jwt', {session: false}), (req, res, next) => {
      // Get all users

      let settings = {
        skip: parseInt(req.query.skip) || 0,
        limit: parseInt(req.query.limit) || 10
      };
      Users.getUsers(settings)
           .then((users) => {
             res.json({status: 'success', users: users});
           })
           .catch(Utils.catchAPIError(res))
           ;
    })
    .post(passport.authenticate('jwt', {session: false}), (req, res, next) => {
      // Create a user
      // console.log("POST: ", req.body);

      // console.log("User: ", req.user);
      if (!(req.user && req.user.Role && req.user.Role.Name == 'Administrator'))
        return Utils.catchAPIError(res)(new httpError(401, "Unauthorized."));

      let settings = Object.assign({}, req.body);

      Users.Create(settings)
        .then((User) => {
          res.json({status: 'success', user: User});
        })
        .catch(Utils.catchAPIError(res))
        ;
    })
    ;

  router.route('/run/:id')
    .get(passport.authenticate('jwt', {session: false}), (req, res, next) => {
      // Get users for run
      Users.getUsersForRun(req.params.id)
        .then((users) => {
          res.json({status: 'success', users: users})
        })
        .catch(Utils.catchAPIError(res))
        ;
    })
    ;

  
  router.route('/:id')
    .get(passport.authenticate('jwt', {session: false}), (req, res, next) => {
      // Get a user
      Users.getById(req.params.id)
        .then((User) => {
          if (User)
            res.json({status: 'success', user: User});
          else
            res.status(404).json({status: 'error', errors: ["User not found."]});
        })
        .catch(Utils.catchAPIError(res))
        ;
    })
    .put(passport.authenticate('jwt', {session: false}), (req, res, next) => {
      // Update a user
      if (!(req.user && req.user.Role && req.user.Role.Name == 'Administrator')) {
        res.status(401).json({status: 'error', errors: ["Unauthorized."]});
      } else {
        Users.Update(req.params.id, req.body, false)
          .then((user) => {
            res.json({status: 'success', user: user});
          })
          .catch(Utils.catchAPIError(res))
          ;
        }
    })
    .patch(passport.authenticate('jwt', {session: false}), (req, res, next) => {
      // Patch update a user
      // Update a user
      if (!(req.user && req.user.Role && req.user.Role.Name == 'Administrator')) {
        res.status(401).json({status: 'error', errors: ["Unauthorized."]});
      } else {
        Users.Update(req.params.id, req.body, true)
          .then((user) => {
            res.json({status: 'success', user: user});
          })
          .catch(Utils.catchAPIError(res))
          ;
        }
    })
    .delete(passport.authenticate('jwt', {session: false}), (req, res, next) => {
      if (!req.user || !req.user.Role || !req.user.Role.Name || (req.user.Role.Name != 'Administrator' && req.user.Role.Name != 'GM'))
        return res.status(401).json({status: 'error', errors: ["Unauthorized"]});

      Users.Delete(req.params.id)
        .then(() => {
          res.json({status: 'success'});
        })
        .catch(Utils.catchAPIError(res))
        ;
    })
    // .get(passport.authenticate('jwt', {session: false}), (req, res, next) => {
    //   // console.log("User: ", req.user);
    //   let settings: any = {
    //     skip: req.query.skip || 0,
    //     limit: req.query.limit || 0
    //   }

    //   Runs.getRuns(settings)
    //     .then((runs) => {
    //       res.json({status: 'success', runs: runs});
    //     })
    //     .catch(Utils.catchAPIError(res))
    //     ;
    // })





  /** Return our Express router, to be hooked to the application. */
  return router;
}

export default Hook;