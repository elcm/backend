import { default as AuthHook } from './auth';
import { default as RunHook } from './run';
import { default as UserHook } from './user';
import { default as ResponseHook } from './runresponse';

/** Handles passing the application to our individual route files, and hooking them to their base endpoint URL.*/
function Hook(app: any): void {
  /** Authentication */
  app.use('/api/auth', AuthHook(app));
  /** Runs */
  app.use('/api/run', RunHook(app));

  /** Run responses */
  app.use('/api/runresponse', ResponseHook(app));

  /** Users */
  app.use('/api/user', UserHook(app));
}

export default Hook;