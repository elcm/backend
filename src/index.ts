require('dotenv').config();

//////////////////////////////////////////////////
// Modules
//////////////////////////////////////////////////
import * as http from 'http';
import * as express from 'express';
import * as path from 'path';
import * as fs from 'fs';
import * as session from 'express-session';
import * as cookieParser from 'cookie-parser';
import * as bodyParser from 'body-parser';
import * as Neode from 'neode';
import * as jwt from 'jsonwebtoken';
import * as httpError from 'http-errors';
import * as cors from 'cors';
import * as morgan from 'morgan';

import { Users } from './lib';

//////////////////////////////////////////////////
// Config
//////////////////////////////////////////////////
/** Express application. */
const app = express();

/** HTTP server */
const server = http.createServer(app);

app.use(morgan('tiny'));

app.use(express.json());
app.use(express.urlencoded({ extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

/** Express session settings */
const sess = {
  /** Secret key */
  secret: process.env.EXPRESS_SECRET,
  /** Cookie settings */
  cookie: {},
  /** Whether or not to forcefully save message back to store, even if it wasn't modified. */
  resave: false,
  /** Force an unitizialized session to be saved to the store. */
  saveUninitialized: true
};

app.use(session(sess));
//////////////////////////////////////////////////
// Database
//////////////////////////////////////////////////
import { db, initialize as initDB } from './lib/db';
app.db = db;
initDB();


//////////////////////////////////////////////////
// Passport Setup
//////////////////////////////////////////////////
import { initialize as initializePassport } from './passport';
import * as passport from 'passport';

app.use(passport.initialize());
initializePassport(passport);

app.passport = passport;


//////////////////////////////////////////////////
// Routes
//////////////////////////////////////////////////
import { default as hookRoutes } from './routes';
hookRoutes(app);


//////////////////////////////////////////////////
// SOcket handlers
//////////////////////////////////////////////////
// import './sockets';


//////////////////////////////////////////////////
// Start
//////////////////////////////////////////////////
server.listen(process.env.EXPRESS_PORT, () => {
  console.log(`Listening on port ${process.env.EXPRESS_PORT}`);
})