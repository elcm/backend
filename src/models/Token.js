module.exports = {
  Token: 'string',
  Created: 'number',
  Type: 'string',
  User: {
    type: 'relationship',
    relationship: 'User',
    target: 'User',
    direction: 'OUT'
  }
}