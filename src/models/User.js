module.exports = {
  _id: {
    type: 'uuid',
    primary: true
  },
  Name: 'string',
  Avatar: 'string',
  Email: 'string',
  Password: 'string',
  Created: 'number',
  Active: 'boolean',

  Role: {
    type: 'relationship',
    relationship: 'Role',
    target: 'Role',
    direction: 'OUT'
  }
}