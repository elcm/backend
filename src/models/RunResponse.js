module.exports = {
  _id: {
    type: 'uuid',
    primary: true
  },
  Title: 'string',
  Description: 'string',
  CharacterLink: 'string',
  RPResponse: 'string',
  Created: 'number',
  Fields: 'string',
  User: {
  	type: 'relationship',
  	relationship: 'User',
  	target: 'User',
  	direction: 'OUT'
  },
  Run: {
    type: 'relationship',
    relationship: 'Run',
    target: 'Run',
    direction: 'OUT'
  }
}