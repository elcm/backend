module.exports = {
  _id: {
    type: 'uuid',
    primary: true
  },
  Name: 'string',
  Created: 'number',
  Description: 'string',
  PlayerMin: {
    type: 'number',
    required: false
  },
  PlayerMax: {
    type: 'number',
    required: false
  },
  Duration: {
    type: 'string',
    required: false
  },
  Communication: {
    type: 'string',
    required: false
  },
  Location: {
    type: 'string',
    required: false
  },
  ThreatLevel: {
    type: 'string',
    required: false
  },
  GameTheme: {
    type: 'string',
    required: false
  },
  GameType: {
    type: 'string',
    required: false
  },
  Prerequisites: {
    type: 'string',
    required: false
  },
  RPPrompt: {
    type: 'string',
    required: false
  },
  Fields: {
    type: 'string',
    required: false
  },

  Owner: {
  	type: 'relationship',
  	relationship: 'Owner',
  	target: 'User',
  	direction: 'OUT'
  }
}