module.exports = {
  _id: {
    type: 'uuid',
    primary: true
  },
  Name: 'string',
  Description: 'string'
}