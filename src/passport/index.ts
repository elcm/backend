// const passport = require('passport');
import * as passport from 'passport';
import * as bcrypt from 'bcryptjs';
import * as Neode from 'neode';
import * as httpError from 'http-errors';

import { Strategy as JwtStrategy } from 'passport-jwt';
import { Strategy as LocalStrategy } from 'passport-local';
import { ExtractJwt } from 'passport-jwt';

import { Users } from '../lib';

/** Options for configuring json web token implementation.*/
const jwtOptions = {
  /** How to retrieve JWT from Expre request object */
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_SECRET,
  /** Issuer address, for security */
  issuer: process.env.JWT_ISSUER,
  /** Audience. */
  audience: process.env.JWT_AUDIENCE
};


function initialize(passport) {
  passport.use(new LocalStrategy((username, password, done) => {
    console.log("Local");
    Users.findByEmailAndPassword(username, password)
      .then((user) => {
        // console.log("Found: ", user);
        if (user)
          done(null, user);
        else
          throw new httpError(401, "Unable to authenticate.");
      })
      .catch((err) => {
        let ret: any = {};
        if (err.statusCode)
          ret.statusCode = err.statusCode;
        ret.message = err.message;

        console.error(err);
        return done(null, false, ret);
      });
  }));

  passport.use(new JwtStrategy(jwtOptions, (payload, done) => {
    Users.lookupFromToken(payload)
      .then((User) => {
        done(null, User);
      })
      .catch((err) => {
        done(err, null);
      })
      ;
  }));

  passport.serializeUser((user, done) => {
    console.log("Serialize: ", user);
    done(null, null);
  });

  passport.deserializeUser((id, done) => {
    console.log("Deserialize: ", id);
    done(null, null);
  });
}

export { initialize, passport }