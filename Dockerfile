FROM node:current-alpine

WORKDIR /backend
COPY package.json .
RUN npm i -g nodemon ts-node mocha typescript && npm install --no-cache
RUN adduser --home /code -D developer && adduser developer users
USER developer:users

CMD ["npm", "start"]

